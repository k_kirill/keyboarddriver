#pragma once

#ifndef GLOBAL_H
#define GLOBAL_H

#include <string>
#include <memory>
#include <exception>
#include <array>

#include <QDateTime>
#include <QApplication>
#include <QMainWindow>
#include <QString>
#include <QTimer>
#include <QWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QDebug>
#include <QThread>
#include <QProcess>

#include "logger.h"

using std::string;
using std::shared_ptr;
using std::exception;
using std::array;

typedef enum { CONCIERGE = 1,
               EMERGENCY = 2,
               RESIDENT = 3 } AbonentType;

typedef enum { OK = 0,
               NO_REGISTRATION = 1,
               NO_NUMBER = 3 } ErrorCode;

typedef enum { Stop = 0 ,
               Start = 1} AudioCommand;

#endif // GLOBAL_H
