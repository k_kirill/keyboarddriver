#ifndef LOGGER_H
#define LOGGER_H

#include"global.h"

class Logger
{
public:
    static void init();
    static QString show();
    static void write(QString);
    static void reset();
    static bool isEmpty();

private:
    static QString message;
    static const QString log_path;
};

#endif // LOGGER_H
