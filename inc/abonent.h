#pragma once

#ifndef ABONENT_H
#define ABONENT_H

#include "global.h"

class Abonent
{

public:
    Abonent() { }
    ~Abonent() { }

public:
    //getters and setters
    void setType(AbonentType type) { this->type = type; }
    AbonentType getType() { return type; }

    void setChosen(bool is_chosen) { this->is_chosen = is_chosen; }
    bool IsChosen() { return is_chosen; }

    void setActive(bool is_active) { this->is_active = is_active; }
    bool IsActive() { return is_active; }

protected:
    AbonentType type;
    bool is_chosen;
    bool is_active;
};
#endif // ABONENT_H
