#ifndef CUSTOMERSERVICE_H
#define CUSTOMERSERVICE_H

#include "global.h"
#include "abonent.h"

class CustomerService : public Abonent
{
public:
    CustomerService(AbonentType);
    ~CustomerService();
};

#endif // CUSTOMERSERVICE_H
