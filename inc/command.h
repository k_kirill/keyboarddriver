#ifndef COMMAND_H
#define COMMAND_H

#include "global.h"

class Command : public exception
{
public:
    static void execute(QString);
    static QString getFromCommand(QString);

private:
    virtual const char* what() const throw() { return "Cannot execute shell command!"; }
};
#endif // COMMAND_H
