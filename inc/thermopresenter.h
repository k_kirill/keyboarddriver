#ifndef THERMOPRESENTER_H
#define THERMOPRESENTER_H

#include "global.h"

class ThermoPresenter : public QObject
{
    Q_OBJECT

public:
    ThermoPresenter();
    ~ThermoPresenter();

public:
    QString getTemperature();

private slots:
    void doWork();

signals:
    void send();

private:
    QString temperature;
};

#endif // THERMOPRESENTER_H
