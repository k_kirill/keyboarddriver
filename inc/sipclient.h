#ifndef SIPCLIENT_H
#define SIPCLIENT_H

#include "global.h"
#include "abonent.h"

class SIPClient : public QObject
{
    Q_OBJECT

public:
    SIPClient();
    ~SIPClient();

public:
    void Call(Abonent*);
    void Terminate();

private:
    void CheckOnErrors();

private:
    ErrorCode err_code = OK;
};

#endif // SIPCLIENT_H
