#ifndef TIMESHOWER_H
#define TIMESHOWER_H

#include "global.h"

class TimePresenter : public QObject
{
    Q_OBJECT

public:
    TimePresenter();
    ~TimePresenter();

public:
    QString getTime();
    QString getDate();

private slots:
    void doWork();

signals:
    void send();

private:
    QString date;
    QString time;
};

#endif // TIMESHOWER_H
