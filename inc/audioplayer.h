#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include "global.h"

class AudioPlayer : public QObject
{
    Q_OBJECT

public:
    AudioPlayer();
    ~AudioPlayer();

public:
    void PlayClickButton();
    void PlayCall(AudioCommand);
};

#endif // AUDIOPLAYER_H
