#ifndef HOLDDETECTOR_H
#define HOLDDETECTOR_H

#include "global.h"

class HoldDetector : public QObject
{
    Q_OBJECT

public:
    HoldDetector();
    ~HoldDetector();

public:
    bool IsHolded();

private:
    void createNotification(int);

private slots:
    void OnGoodNotify();
    void OnErrorNotify();
    void OnSendedKey(int);

signals:
    void send_hold();
    void send_good();

private:
    QTime timer;
    QTimer* t;
    QTimer* t1;
    int key = -1;
    int prev_time_key_press = -1;
    int sum_time = 0;
    bool is_holded = false;
};

#endif // HOLDDETECTOR_H
