#ifndef VIEW_H
#define VIEW_H

//System part
#include "global.h"
#include "timepresenter.h"
#include "thermopresenter.h"
#include "holddetector.h"
#include "audioplayer.h"

//Client part
#include "sipclient.h"
#include "resident.h"
#include "customerservice.h"

class View : public QWidget
{
    Q_OBJECT

public:
    explicit View(QWidget *parent = 0);
    ~View();

private:
    shared_ptr<CustomerService> concierge;
    shared_ptr<CustomerService> emergency;
    shared_ptr<Resident> resident;

    TimePresenter* time_presenter;
    ThermoPresenter* thermo_presenter;
    HoldDetector* detector;
    SIPClient* sip_client;
    AudioPlayer* player;

    QThread* thread1;
    QThread* thread2;
    QThread* thread3;
    QThread* thread4;

    QTimer* timer;

private:
    void addSymbol(char symbol);
    void updateTimer();

signals:
    void sendKey(int);

private slots:
    void OnResetEvent();
    void OnUpdateDateTimeEvent();
    void OnUpdateTemperatureEvent();

    void OnHoldedKeyBoardEvent();
    void OnGoodKeyBoardEvent();

protected:
    void paintEvent(QPaintEvent*);
    void keyPressEvent(QKeyEvent*);
};

#endif // VIEW_H
