#ifndef RESIDENT_H
#define RESIDENT_H

#include "global.h"
#include "abonent.h"

class Resident : public Abonent
{
public:
    Resident();
    ~Resident();

public:
    void setNumber(QString);
    QString getNumber();

private:
    QString number;
};

#endif // RESIDENT_H
