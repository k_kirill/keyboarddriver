#include "resident.h"

Resident::Resident() : Abonent()
{
    this->type = RESIDENT;
    this->is_chosen = false;
    this->is_active = false;
}

Resident::~Resident() { }

void Resident::setNumber(QString number) { this->number = number; }

QString Resident::getNumber() { return number; }

