#include "thermopresenter.h"
#include "command.h"

ThermoPresenter::ThermoPresenter()
{

}

ThermoPresenter::~ThermoPresenter()
{

}

void ThermoPresenter::doWork()
{
    while (true)
    {
        temperature = Command::getFromCommand("cat /run/thermal_sensor/external_temp");
        emit send();
        QThread::sleep(1800);
    }
}

QString ThermoPresenter::getTemperature()
{
    return temperature;
}
