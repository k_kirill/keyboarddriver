#include "sipclient.h"
#include "resident.h"
#include "command.h"

SIPClient::SIPClient()
{
    Command::execute("/opt/bin/scripts/startup_sip.sh");
}

SIPClient::~SIPClient()
{

}

void SIPClient::Call(Abonent* abonent)
{
    CheckOnErrors();

    if(err_code != OK)
    {
        Logger::write("Error: code = " + QString::number(err_code));
        return;
    }

    if(abonent->getType() == RESIDENT)
    {
        Resident *resident = static_cast<Resident*>(abonent);
        QString number = resident->getNumber();

        QString result = Command::getFromCommand("/opt/bin/scripts/call.sh " + number);
        err_code = static_cast<ErrorCode>(result.toInt());

        if(err_code != OK)
            Logger::write("Error: code = " + QString::number(err_code));

        else
            Logger::write("Calling " + number);

        return;
    }

    if(abonent->getType() == CONCIERGE)
    {
        QString result = Command::getFromCommand("/opt/bin/scripts/concierge.sh");
        err_code = static_cast<ErrorCode>(result.toInt());

        if(err_code != OK)
            Logger::write("Error: code = " + QString::number(err_code));

        else
            Logger::write("Calling\n concierge...");

        return;
    }

    if(abonent->getType() == EMERGENCY)
    {
        QString result = Command::getFromCommand("/opt/bin/scripts/emergency.sh");
        err_code = static_cast<ErrorCode>(result.toInt());

        if(err_code != OK)
            Logger::write("Error: code = " + QString::number(err_code));

        else
            Logger::write("Calling\n emergency...");

        return;
    }

    return;
}

void SIPClient::Terminate()
{
    Command::execute("/opt/bin/scripts/terminate.sh");
}

void SIPClient::CheckOnErrors()
{
    QString result = Command::getFromCommand("/opt/bin/scripts/sip_validation.sh");
    int status = result.toInt();

    if(status == OK)
        err_code = OK;

    if(status == NO_REGISTRATION)
        err_code = NO_REGISTRATION;
}
