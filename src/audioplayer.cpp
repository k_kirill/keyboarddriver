#include "audioplayer.h"
#include "command.h"

AudioPlayer::AudioPlayer()
{

}

AudioPlayer::~AudioPlayer()
{

}

void AudioPlayer::PlayClickButton()
{
    Command::execute("/opt/bin/scripts/play_click.sh");
    //Command::execute("aplay /home/kirill/workspace/sounds/click.wav &");
}

void AudioPlayer::PlayCall(AudioCommand comm)
{
    QString comm_str = QString::number(comm);

    Command::execute("/opt/bin/scripts/play_call.sh " + comm_str);
    //Command::execute("aplay /home/kirill/workspace/sounds/call.wav &");

}
