#include "view.h"

#ifdef Q_OS_LINUX

#include <unistd.h>

#endif

int main(int argc, char *argv[])
{
    //Make application as service
//#ifdef Q_OS_LINUX

  //  daemon(0, 0);

//#endif

    QApplication a(argc, argv);
    View view;
    view.show();

    return a.exec();
}
