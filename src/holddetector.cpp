#include "holddetector.h"
#include "command.h"

HoldDetector::HoldDetector()
{
    t = new QTimer(this);
    t1 = new QTimer(this);
    timer.start();

    connect(t, SIGNAL(timeout()), this, SLOT(OnGoodNotify()));
    connect(t1, SIGNAL(timeout()), this, SLOT(OnErrorNotify()));
}

HoldDetector::~HoldDetector()
{
    delete t;
    delete t1;
}

void HoldDetector::OnSendedKey(int k)
{
    if(!is_holded)
    {
        if(key == k)
        {
            int current_time = timer.elapsed();
            int interval = current_time - prev_time_key_press;
            prev_time_key_press = current_time;

            if(interval <= 100)
            {
                if(sum_time < 5000)
                {
                    sum_time += interval;
                }

                else
                {
                    is_holded = true;
                    t->start(10000);
                    t1->start(30000);
                    createNotification(key);
                    emit send_hold();
                }
            }

            else
            {
                sum_time = 0;
            }
        }

        else
        {
            key = k;
            prev_time_key_press = timer.elapsed();
        }
    }

    else
    {
        if(key == k)
        {
            t->stop();
            t->start(10000);
        }
    }
}

void HoldDetector::OnGoodNotify()
{
    if(t->isActive()) t->stop();

    if(t1->isActive()) t1->stop();

    is_holded = false;
    sum_time = 0;
    emit send_good();
}

void HoldDetector::OnErrorNotify()
{
    Command::execute("/opt/bin/scripts/error_notify.sh");
}

bool HoldDetector::IsHolded()
{
    return is_holded;
}

void HoldDetector::createNotification(int key)
{
    QString info;
    QString message;

    if (key >= Qt::Key_0 && key <= Qt::Key_9)
        info.append('0' + key - Qt::Key_0);

    switch (key)
    {
    //emergency
    case 0x1000030:
        info = "emergency";
        break;
        //concierge
    case 0x1000031:
        info = "concierge";
        break;
        //Call
    case 0x1000032:
        info = "call";
        break;
        //Reset
    case 0x1000000:
        info = "reset";
        break;
        //default
    default:
        break;
    }

    message = "Error:" + info + "\n is holding!";
    Logger::write(message);
}
