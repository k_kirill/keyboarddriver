#include "timepresenter.h"

TimePresenter::TimePresenter()
{

}

TimePresenter::~TimePresenter()
{

}

void TimePresenter::doWork()
{
    while (true)
    {

        date = QDateTime::currentDateTime().toString("d MMMM");
        time = QDateTime::currentDateTime().toString("hh:mm");

        emit send();
        QThread::sleep(5);
    }
}

QString TimePresenter::getTime()
{
    return time;
}

QString TimePresenter::getDate()
{
    return date;
}
