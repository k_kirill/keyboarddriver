#include "command.h"

void Command::execute(QString command)
{
    try
    {
        QByteArray arr = command.toLatin1();
        system(arr.data());
    }

    catch(exception& ex)
    {
        qDebug() << ex.what();
    }
}

QString Command::getFromCommand(QString command)
{
    QString result = "";

    try
    {
		QProcess process;
		process.start(command);

		if (!process.waitForStarted() || !process.waitForFinished())
			return "Error body command!";

		if (process.readAllStandardError().isEmpty())
			result = process.readAllStandardOutput();

		else
			result = process.readAllStandardError();
        //QByteArray arr = command.toLatin1();
        //const char* cmd = arr.data();

        //array<char, 128> buffer;
        //shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);

        //if (!pipe)
        //    throw std::runtime_error("popen() failed!");

        //while (!feof(pipe.get()))
        //{
        //    if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
        //        result += buffer.data();
        //}
        return result;
    }

    catch(exception& ex)
    {
        qDebug() << ex.what();
        return result;
    }
}
