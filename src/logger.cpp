#include"logger.h"
#include "command.h"

QString Logger::message;
const QString Logger::log_path = "/opt/bin/log/keyboardDriver.log";

void Logger::init()
{
    message = "";
    Command::execute("/opt/bin/scripts/clear_log.sh &");
}

QString Logger::show()
{
    return message;
}

void Logger::write(QString msg)
{
    message = msg;
    QString date_time = QDateTime::currentDateTime().toString("d.MM.yyyy");
    Command::execute("echo " + date_time + " " + message + ">> " + log_path);
}

void Logger::reset()
{
    message = "";
}

bool Logger::isEmpty()
{
    if(message == "")
        return true;

    else
        return false;
}
