#include "view.h"

View::View(QWidget *parent) :
    QWidget(parent)
{
    setFixedSize(128, 64);
    Logger::init();

    //Keyboard timer
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(OnResetEvent()));

    concierge = std::make_shared<CustomerService>(CONCIERGE);
    emergency = std::make_shared<CustomerService>(EMERGENCY);
    resident = std::make_shared<Resident>();

    //Will update time and temperature in other threads
    time_presenter = new TimePresenter();
    thermo_presenter = new ThermoPresenter();

    //Hold detector
    detector = new HoldDetector();

    //Initialize and register SIP client
    sip_client = new SIPClient();

    //Audio player
    player = new AudioPlayer();

    thread1 = new QThread();
    thread2 = new QThread();
    thread3 = new QThread();
    thread4 = new QThread();

    time_presenter->moveToThread(thread1);
    thermo_presenter->moveToThread(thread2);
    detector->moveToThread(thread3);
    sip_client->moveToThread(thread4);

    //Time presenter thread
    connect(time_presenter, SIGNAL(send()), this, SLOT(OnUpdateDateTimeEvent()));
    connect(thread1, SIGNAL(started()), time_presenter, SLOT(doWork()));

    //Thermo presenter thread
    connect(thermo_presenter, SIGNAL(send()), this, SLOT(OnUpdateTemperatureEvent()));
    connect(thread2, SIGNAL(started()), thermo_presenter, SLOT(doWork()));

    //Hold detector thread
    connect(detector, SIGNAL(send_hold()), this, SLOT(OnHoldedKeyBoardEvent()));
    connect(detector, SIGNAL(send_good()), this, SLOT(OnGoodKeyBoardEvent()));
    connect(this, SIGNAL(sendKey(int)), detector, SLOT(OnSendedKey(int)));

    thread1->start();
    thread2->start();
    thread3->start();
    thread4->start();
}

View::~View()
{
    delete time_presenter;
    delete thermo_presenter;
    delete detector;
    delete timer;
    delete player;

    delete thread1;
    delete thread2;
    delete thread3;
    delete thread4;
}

void View::addSymbol(char symbol)
{
    QString number = resident->getNumber();

    if (number.count() < 4)
    {
        number.append(symbol);
    }

    else
    {
        for(auto ch = number.end(); ch != number.begin(); --ch)
        {
            *ch = *(ch - 1);

            if(ch == number.begin() + 1)
                *(ch - 1) = symbol;
        }
    }

    resident->setNumber(number);
}

void View::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::TextAntialiasing, false);

    if( !resident->IsChosen() &&
        !concierge->IsChosen() &&
        !emergency->IsChosen() &&
        !detector->IsHolded() )
    {
         QFont font1("Terminal", 14);
         painter.setFont(font1);
         painter.drawText(rect(), Qt::AlignTop | Qt::AlignHCenter, time_presenter->getTime());

         QFont font2("Terminal", 12);
         painter.setFont(font2);
         painter.drawText(rect(), Qt::AlignCenter, time_presenter->getDate());

         QFont font3("Terminal", 12);
         painter.setFont(font3);
         painter.drawText(rect(), Qt::AlignBottom | Qt::AlignHCenter, thermo_presenter->getTemperature());
         return;
    }

    if (resident->IsChosen() && !resident->IsActive())
    {
        QFont font("Terminal", 24);
        painter.setFont(font);
        painter.drawText(rect(), Qt::AlignCenter, resident->getNumber());
        painter.end();
    }

    else
    {
        QFont font("Terminal", 12);
        painter.setFont(font);
        painter.drawText(rect(), Qt::AlignCenter, Logger::show());
        painter.end();
    }
}

//Press button
void View::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();
    emit sendKey(key);

    if(!detector->IsHolded())
        player->PlayClickButton();

    else
        return;

    if ((key >= Qt::Key_0 && key <= Qt::Key_9) &&
        (!emergency->IsChosen() || !concierge->IsChosen()))
    {
        if(!resident->IsChosen())
            resident->setChosen(true);

        addSymbol('0' + key - Qt::Key_0);
        updateTimer();
    }

    //Key buttons
    switch (key)
    {
    case 0x1000030:
        if( !emergency->IsChosen() &&
            !resident->IsChosen() &&
            !concierge->IsChosen() &&
            !detector->IsHolded() )
        {
            emergency->setChosen(true);
            Logger::write("Will you call\nemergency?");
            updateTimer();
        }
        break;
    case 0x1000031:
        if( !emergency->IsChosen() &&
            !resident->IsChosen() &&
            !concierge->IsChosen() &&
            !detector->IsHolded() )
        {
            concierge->setChosen(true);
            Logger::write("Will you call\nconcierge?");
            updateTimer();
        }
        break;
    case 0x1000032:
        if( (resident->IsChosen()    ||
             emergency->IsChosen()  ||
             concierge->IsChosen()) && !detector->IsHolded() )
        {
            if(timer->isActive()) { timer->stop(); }

            QThread::msleep(200);
            player->PlayCall(Start);

            if(resident->IsChosen() && !resident->IsActive())
            {
                resident->setActive(true);
                sip_client->Call(resident.get());
                break;
            }

            if(emergency->IsChosen() && !emergency->IsActive())
            {
                emergency->setActive(true);
                sip_client->Call(emergency.get());
                break;
            }

            if(concierge->IsChosen() && !concierge->IsActive())
            {
                concierge->setActive(true);
                sip_client->Call(concierge.get());
                break;
            }
        }
        break;
    case 0x1000000:
        OnResetEvent();
        break;
    }

    update();
}

void View::updateTimer()
{
    //Stop and start new
    if(timer->isActive()) { timer->stop(); }
    timer->start(10000);
}

void View::OnResetEvent()
{
    QThread::msleep(200);
    player->PlayCall(Stop);

    if(!detector->IsHolded())
    {
        Logger::reset();
        resident->setNumber("");

        sip_client->Terminate();

        if(concierge->IsChosen()) { concierge->setChosen(false); }
        if(emergency->IsChosen()) { emergency->setChosen(false); }
        if(resident->IsChosen())  { resident->setChosen(false);  }

        if(concierge->IsActive()) { concierge->setActive(false); sip_client->Terminate(); }
        if(emergency->IsActive()) { emergency->setActive(false); sip_client->Terminate(); }
        if(resident->IsActive())  { resident->setActive(false);  sip_client->Terminate(); }
    }
}

void View::OnUpdateDateTimeEvent()
{
    update();
}

void View::OnUpdateTemperatureEvent()
{
    update();
}

void View::OnHoldedKeyBoardEvent()
{
    if( emergency->IsChosen() ||
        resident->IsChosen() ||
        concierge->IsChosen() )
    {
        concierge->setChosen(false);
        concierge->setActive(false);

        emergency->setChosen(false);
        emergency->setActive(false);

        resident->setChosen(false);
        resident->setActive(false);
    }

    if(timer->isActive())
        timer->stop();

    update();
}

void View::OnGoodKeyBoardEvent()
{
    OnResetEvent();
}
